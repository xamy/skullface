# About me

![](../images/avatar-photo.jpg)

Hello again! I guess the index page was just interesting enough to warrant a look at who I am and what I do!
Despite what it says in my ID, my name is Alexey. I'm a 33 year old, self-employed graphics designer and self-proclaimed specialist in advertisement and marketing who dabbles in all sorts of technical and technological fields - primarily in the field of educational media consumption :^)

## My background

It all began on a sunny afternoon in the cold siberian city named "Novy Urengoi". Born not too long before the Chernobyl disaster, **_but totally not responsible for it_**, I was a quiet kid that just played with construction toys and was often stuck with repairing toys my brother and sister broke before I got it. My affinity for drawing and general creativity somehow brought me into the field of advertisement early in my teen years, and I've stuck with it since. An unhealthy dose of interest for technology, engineering and _things that do stuff_, eventually motivated me to enroll in HSRW for their Communication and Information Engineering B.Sc. course.

## Previous work

I'll add my Projects from the two Accessathon attendances later! Meanwhile, enjoy a couple of pictures of my cats!

![The cutest cat ever](../images/nyu.jpg) ![Wait this one is cuter even](../images/leeloo.jpg)

### Project "One Click Alien Defense"

TBA​

### Project "Braillotron"

TBA
