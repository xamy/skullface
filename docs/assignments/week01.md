# 1. Project Management

## Summary

This week we learned how to use the GIT version control system, its workflow and how to use Markdown to make project documentation quick and easy.

## Documenting Progress

**_Pics or it didn't happen_** is not only an internet rule, but a rule in all science - if one cannot provide a detailed report on one's findings or innovations, one cannot pass on their knowledge (effectively, anyway). The first week's lesson was all about learning on how to document our course progression in a way that was convenient for ourselves to create and update and for our sensei to check up on. Version control (provided by [**gitlab**](https://gitlab.com)) was to ensure a non-destructive way of taking notes of our progress without worry of massive loss of data (with the additional bonus of providing some free webspace for the project documentation website) and [**Markdown**](https://daringfireball.net/projects/markdown/) would provide an easy way to make and edit said website.

### Using GIT

Git is **the** most popular version control system in the world. Created by the legendary Linus Torvalds for his own projects, git is now used by _everyone_.

>What does **version control** _mean_ though?

Version control means the git software is observing your project files for changes and makes _versions_ of those files as you continue your work (_though it does work for many other forms of gradual progression, like creation of art assets, as well_), **backing up every version** of work you create and _commit_ to it. This is useful as you may want to "roll back" to an older, working version of your project in various cases.

### Using BASH

Like many other GNU/Linux-based software solutions, **BASH** has no graphical user interface and works solely via a command line interface. During this week's course, we learned some basics on it's use, a few directory and file manipulation commands and how to use git-specific commands to index and upload our progress.

![BASH window showing "git status" summary of this project. Nothing of particular interest](../../images/week01/bash.png)

From memory, here are a few commands I still remember:

| Command | Decoded | Description |
| --- | --- | --- |
| ``` pwd ``` | "print working directory" | Output a path to the folder ("directory") you're currently working in |
| ``` cd folder/path/ ``` | "change directory" | Changes working directory to the one specified after the space (using ".." instead of a folder name will move you one folder up the tree) |
| ``` mkdir path/foldername ``` | "make directory" | Creates a specified directory in the specified path |
| ``` cp path/file.name path/newfile.name ``` | "copy" | Copies a file or folder to a specified location |
| ``` mv path/file.name path/newfile.name ``` | "move" | Copies a file or folder to a specified location and deletes the original |
| ``` rm path/file.name ``` | "remove" | Deletes a file or folder in a specified location |


GIT has it's own set of commands and arguments to manage your project files. The following are the ones we will be using the most (Descriptions are what I've understood the commands are doing...):

| Command | Command arguments | Description |
| --- | --- | --- |
| ``` git status ``` | - | Provides a status summary of a git-managed folder if your working directory is one. |
| ``` git add ``` | ``` filenames / * ``` | Adds separate files or entire folders and subfolders to the index. |
| ``` git rm ``` | ``` filenames / * ``` | Removes separate files or entire folders and / or subfolders from the index. |
| ``` git commit ``` | - | Creates the necessary file links for version control of your progress. |
| ``` git pull ``` | ``` -m "message" ``` | Merges the remote repository into the local folder structure. The message helps tracking the changes made to the project. |
| ``` git push ``` | ``` project branch ``` | Merges the committed local files into the remote repository & updates the remote folder structure. |

## Weekly Assignment

This week's assigment was to practice what we've learned and to create a webpage with our project documentation.