# 2. 2D and 3D Design

## Summary

This week we learned how to use two software suites for 2D and 3D design using which we will be conducting our engineering efforts.

## I love it when a plan comes together

**Measure twice, cut once** is the old saying, and in engineering, it's all about a good amount of prep work. For our future fabrication needs, we will be using [LibreCAD](), an open source 2D cad tool with a wide variety of functions that is also pretty easy to use; and [Fusion 360](), a commercial engineering suite with the power to model fully assembled machinery, do animations, simulations and much more.

## Using LibreCAD

LibreCAD, being a free, open-source utility, is not a piece of art to look on. However, its intuitive user interface and shortcuts make it a powerful and precise 2D design tool, especially for [laser cutting](../assignments/week03/).
It has all the neccessary line, shape and manipulation tools to create the right building blocks for our fabrication purposes.

![Screenshot of LibreCAD, featuring a design saying "Fun the mentals - imported from illustrator™"](../../images/week02/LibreCAD.png)

If you want to try it, here are some of the basics:

| Button | Description |
| ---- | ---- |
| ![Cropped screenshot of the shape creation buttons](../../images/week02/creationtools.png) | Using these tools, you can create nearly every imaginable 2D shape. |
| ![Cropped screenshot of the shape manipulation buttons](../../images/week02/manipulationtools.png) | Manipulation tools allow you to adjust your already created lines in various ways, like moving, rotating, cloning and mirroring lines orentire drawings. |
| ![Cropped screenshot of the shape creation buttons](../../images/week02/alignmenttools.png) | Alignment tools help with the positioning of vertices while creating a precise drawing. |
| ![Cropped screenshot of the shape creation buttons](../../images/week02/creationtools.png) | Axis aligment buttons will make sure your vertice placement tool remains on either the X or Y axis after you have defined an origin point during editing. |

LibreCAD is powerful, free, and easy to learn. If you're serious about engineering, you should get familiar with it.

## Using Fusion 360

Industry-leader level complexity with an enduser-friendly interface? That's [AutoCAD Fusion 360](https://www.autodesk.com/products/fusion-360/) (queue a fun and melodic advertisement jingle). Fusion is remarkably powerful and even more remarkably intuitive to use. It supports the entire design to manufacturing cycle in one package and luckily, for us students, it's use is free and unlimited. Having already worked with Cinema 4D for various design and visualisation purposes, using Fusion seems like a breeze.

![Screenshot of Fusion 360, featuring a couple simple extruded objects.](../../images/week02/Fusion.PNG)

I'm not going to try and explain you the basics as I usually do (maybe some other time), so if you want an introduction, I suggest you check out this video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/qvrHuaHhqHI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Weekly Assignment

Apart from documenting our lecture and work, this week we're going to prepare a 2D and 3D design using the two software suites above. At first I thought to make something quick and easy, like a small pencil box or an cool ultrathin laptop carrying case, but my new laptop isn't even paid for yet and I don't even use pencils anymore.

So I proceeded with doing something for my [final project].

![Screenshot of Illustrator, showing the completed vector line inside a dark cross-section of a skull.](../../images/week02/mdf-import.jpg)
First, I scanned the upper cross-section of an educational skull model I bought a while ago and imported the image into Illustrator, my go-to tool for all things Vector, and created a shape on the inside line of the "bone". I was careful to make the line close to the edge, but the scan was a little muddy so I might be off on a couple edges, but it should not pose a problem later. Before exporting to .DXF so I could create a precise laser cut schematic in the next step, I made sure the scale was spot on - unfortunately between the scanner and the imported PDF, something went awry and the image was 2cm smaller than intended. I had to adjust the dimensions to the correct values I measured myself using a simple ruler.

![Screenshot of LibreCAD with the completed cutout](../../images/week02/mdf-cutout.gif)
Once imported in LibreCAD, I checked the measurements again - they aligned perfectly. I moved the cross-section around several times before settling on a centered position with the origin point at the bottom center of the outline. Using mostly the command line tool and two-point line placement while snapping to the grid, I created a cutout for an optimal position for two independent [LED matrix driver PCBs](https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-matrix) as well as a small cutout for a MDF piece to fix the 3D printed PCB mounts in place. The remaining hole will also be useful to pull out the MDF out of the skull later.

[The results can be downloaded here.](../../assets/crosssection-mounting.dxf)

![Fusion 360 - Sketching out the PCB mount](../../images/week02/pcbmount-1.jpg)
The next step is to make something in 3D using Fusion 360. I went for a neat, perpendicular PCB mount. First, I sketched out a plate for the PCB with appropriate holes for cables and such as well as 4 precisely placed cylinders for the PCB mounting holes. I want it to hold in place tightly but be easy to disassemble (no screws) later.

![Fusion 360 - Extruding everything](../../images/week02/pcbmount-2.jpg)
After Extruding one part after another, the whole thing took shape of what I had imagined - neat! The first version was meant to be a single piece, but I realized that it would not be printable with the overhanging parts, so I split the design in two pieces. One of them would still not be doable using a conventional [FDM](https://de.wikipedia.org/wiki/Fused_Deposition_Modeling) printer, but I figured I might as well mix it up a bit and leave it in since it could still be done with Stereolithography. In the end, the clip that slides into the MDF part is not even really neccessary.

![Fusion 360 - Finalizing the 3D design](../../images/week02/pcbmount-3.jpg).
After finishing I mirrored both pieces and rotated one of the copies to make a nice preview. They all should fit together nicely if the print is tight enough. If you want, you can [view or download the 3D Model here](https://a360.co/2qfIZc0).

Well this was fun! I look forward to making these in the Fablab.

[final project]: ../../projects/final-project/