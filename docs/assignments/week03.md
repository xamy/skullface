# 3. Laser Cutting

## Summary

This week we learned about lasers! Pew pew! We learned how to set everything up and at the end each of us had some laser-cut things and lots of cinder on our hands.

## Lasers and why we love them

Imagine a magical sword that can cut through anything! Now forget it, because lasers aren't really like that. A laser is a machine that emits **L**ight through a process of optical **A**mplification by **S**timulated **E**mission of electromagnetic **R**adiation. _I don't really get it myself yet_, but basically, it's a supercharged beam of light that can be bundled and focused really tightly, allowing you to precisely burn through material with no _physical contact_. Add some motors, some belts, some well-machined pieces of metal and a computer as well as a bunch of smart people to make it all work together and you get a fantastic tool for fabrication, art and **fun**. _But!_ Even though lasers are **fun**, they are also extremely dangerous. Don't just toy around with lasers if you value your vision and extremities!

## Types of Lasers

I want to expand this section for reference some time later, but for now, you can [read this page](https://en.wikipedia.org/wiki/Laser#Types_and_operating_principles).

## The _kerf_ question

The thing about lasers is that contrary to popular belief they're not exactly doing a hairline-cut. They're removing a significant portion of material just like a saw or drill would - except lasers evaporate it instead of scratching it. The amount of material to be evaporated depends on a few factors - power, speed and frequency dictate how deep the laser will be able to "shoot", but the width of the cut - the so-called **kerf** - depends on the maximum width of the laser beam and thickness of material it shoots into as well as the position of the focal point within the material.

>**Kerf should be determined before designing an interlocking design because it will affect how well two parts will fit together**

This week we learned on how to use Fusion's parameter functions to design dynamic blueprints that would adjust the positions of the lines according to our "kerf" parameters.

## Uses for fabrication

Lasers are often used for prototyping because of their speed and precision. There are mass-production lasers as well out there and those are used for commercial applications.

![Stock photo of a large amount of technical parts being laser-cut](../../images/week03/cutting-example.jpg)
[Photo courtesy of pressfoto (freepik.com)](https://www.freepik.com/free-photos-vectors/business)

## Weekly Assignment

This week we get to practice laser-cutting ourselves. After a tutorial in Fusion and a brief intro to the available machines and the laser settings we were allowed to create our own stuff to be engraved and cut on the "small" FabLab lasers.

Since I didn't have my _prop_ for the [final project] with me this week, I decided to make an engraved, interlocking laptop stand using the 5mm MDF board we have had used during the intro.

Using what we learned earlier, I've made a simple but elegant laptop stand design specifically for my 15" laptop with a built-in storage box for the power supply and other stuff.

![Screenshot of Fusion, showing the blueprint for the laptop stand](../../images/week03/vaiostand1.jpg)

I went with a clever interlocking design where each part locks the previous one, making an extremely robust final product. When I was finally ready to cut it all out, I added a fancy engraving using Illustrator (which resulted in a few problems during cutting which required some debugging in "Rhino"). After a lengthy conversion / setup process, it finally worked. I ended up not being able to use the color assignment functionality and used the general settings for each part of the process. First run was engraving the design (which took the longest - almost half an hour) using 100% speed and mere 20% power @ 50% frequency. The subsequent cuts from the inside outwards were cut using 3% speed and 100% power @ 50% frequency. 

Here's a timelapse of the cutting process:

<iframe width="800" height="450" src="https://www.youtube.com/embed/lGFt-5yuTJU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The final product was quite a tight fit - I haven't tested the kerf at this material thickness so I foolishly (and luckily) guessed a kerf of .2mm, so I had to clean up a couple edges with some sand paper. In the end, the whole thing came out perfectly.

![Two pictures of the final product side-by-side, with the laptop in place on the right.](../../images/week03/vaiostand2.jpg)

This was very fun and fulfilling to do, despite the few mishaps during the addition of the engraving.

[You can view and download the Blueprints for the laptop stand here](https://a360.co/2BymG3x) 

[final project]: ../../projects/final-project/

