# 4. Electronics Production

## Summary

This week we learned about PCBs and how to make them using Autodesk Eagle, mspaint and a milling machine as well as the basics of soldering of surface-mounted components. 

## Use your brain imagination!

Planning your IC - how to go from a spec sheet to a PCB layout.

## PCB production

Using various tools to produce a PCB - exporting, preparing, converting and milling a custom layout.

## Soldering

Soldering techniques and what to look out for.

## IC programming

Using the arduino IDE.

## Weekly Assignment

Making a PCB and flashing the attiny85 with the blink program.


[final project]: ../../projects/final-project/

