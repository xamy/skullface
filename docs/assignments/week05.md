# 5. 3D Printing

## Summary

I missed out on this week, but basically, it was all about making 3D objects using additive and substractive fabrication techniques.

## Making it real

How to prepare a 3D model for fabrication using 3D printers.

## Slicin'n'dicin

Using CURA to create proper GCode.

## Weekly Assignment

Making a model and printing it in the fablab.


[final project]: ../../projects/final-project/

