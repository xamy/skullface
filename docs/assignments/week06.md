# 6. Electronics Design

## Summary

This week, we learned about how to use Autodesk Eagle, the industry-standard PCB and electronics design software to create a custom PCB. We also learned some basics about individual types of components and their use cases.

## Put a pin in it

Planning stage in Eagle explained - adding components and routing their connections via labeling.

## Get physical

Drawing the traces to lay out the components on the board using Eagle.

## Weekly Assignment

Designing and fabricating a working PCB.


[final project]: ../../projects/final-project/

