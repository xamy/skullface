# Home

## Hello, world!

![](./images/index.jpg)

## What you'll find here

On this site, I will document my experiences and assignments in the [Fundamentals of Fabrication]() course at [Hochschule Rein-Waal](https://www.hochschule-rein-waal.de). Despite what you might think, this is a serious matter and the actual project documentation might not be very fun to read - unless you're the kind of person that thinks spreadsheets and graphs are fun!